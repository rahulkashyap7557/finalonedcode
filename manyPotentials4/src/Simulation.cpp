#include "allIncludes.h"

// All the simulation functions are written in the header file because
// they were defined as template functions.


Simulation::Simulation()
{
    //ctor
    timeStep = 0.00001;
    samplingFrequency = int(1/timeStep);
    totalTime = 10000;
    method = "gear5";
    systemSize = 100;

}

Simulation::~Simulation()
{
    //dtor
}

Simulation::Simulation(const Simulation& other)
{
    //copy ctor
    timeStep = other.timeStep;
    samplingFrequency = other.samplingFrequency;
    totalTime = other.totalTime;
}

Simulation& Simulation::operator=(const Simulation& rhs)
{
    if (this == &rhs) return *this; // handle self assignment
    //assignment operator
    timeStep = rhs.timeStep;
    samplingFrequency = rhs.samplingFrequency;
    totalTime = rhs.totalTime;

    return *this;
}

void Simulation::Setmethod(string val)
{
    method = val;
}

void Simulation::SetsystemSize(int val)
{
    systemSize = val;
}



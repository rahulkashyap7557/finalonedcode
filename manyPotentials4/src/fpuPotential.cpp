#include "allIncludes.h"

fpuPotential::fpuPotential()
{
    //ctor
    k1 = 0.0;
    k2 = 0.0;
    k3 = 1.0;

}

fpuPotential::fpuPotential(Particle chainParticles)
{
    //ctor


}

fpuPotential::~fpuPotential()
{
    //dtor
}

fpuPotential::fpuPotential(const fpuPotential& other)
{
    //copy ctor
    k1 = other.k1;
    k2 = other.k2;
    k3 = other.k3;
}

fpuPotential& fpuPotential::operator=(const fpuPotential& rhs)
{
    if (this == &rhs) return *this; // handle self assignment
    //assignment operator
    k1 = rhs.k1;
    k2 = rhs.k2;
    k3 = rhs.k3;

    return *this;
}

void fpuPotential::f_accel(std::vector<Particle> &chainParticles)
{
    double fac;
    double fac1;
    double fac2;
    double fac12;
    double fac22;
    double fac23;
    double fac13;
    double mass;
    double a = 0.0;
    int N = chainParticles.size() - 1;
    double dxi, twodxi, dxipl1, dximn1, chainlength;
    string lb, rb;
    chainlength = chainParticles.size();

    lb = chainParticles.at(0).Getlboundary();
    rb = chainParticles.at(N).Getrboundary();


	unsigned int i, k;
	int j;


		for (i = 0; i < chainParticles.size(); i++) {
		j = i - 1;
		k = i + 1;
//		if (j == -1) {
//
////			fac1 = f_leftBoundaryConditionsAccel(chainParticles);
////			cout << fac1 << "first" << endl;
////
//			fac1 = chainParticles.at(i).Getposition();
//			if (lb == "open"){
//                fac1 = fac1 - chainParticles.at(i).Getposition();
//			}
//			if (lb == "periodic"){
//                fac1 = fac1 - chainParticles.at(N).Getposition();
//			}
////			cout << fac1 << "second" << endl;
//
//		} else {
//			fac1 = chainParticles.at(i).Getposition() - chainParticles.at(j).Getposition();
//		}
//
//		if (k == chainParticles.size()) {
//
//            fac2 = chainParticles.at(i).Getposition();
//            if (rb == "open"){
//                fac2 = fac2 - chainParticles.at(N).Getposition();
//            }
//            if (rb == "periodic"){
//                fac2 = fac2 - chainParticles.at(0).Getposition();
//            }
////            cout << setprecision(30) << fac1 << "first" << endl;
//
////		    cout << setprecision(30) << fac1 << "second" << endl;
//
//
//
//		} else {
//			fac2 = chainParticles.at(i).Getposition() - chainParticles.at(k).Getposition();
//		}
//
//		fac = fac1 + fac2;
//
//		fac13 = fac1 * fac1 * fac1;
//		fac23 = fac2 * fac2 * fac2;
//		mass = chainParticles.at(i).Getmass();
//		a = -2.0*k1*fac - 4.0*k2*(fac13+fac23);
////		a = a/mass;
//
//		chainParticles[i].Setaccel(a);
        if (j == -1) {
			dximn1 = 0.0;
        if(lb == "open")
        {
            dximn1 = dxi;
        }
        else if (lb == "periodic")
        {
            dximn1 = chainParticles.at(N).Getposition();
        }
		} else {
			dximn1 = chainParticles.at(j).Getposition();
		}
		if (k == chainlength) {
			dxipl1 = 0.0;
        if (rb == "open")
        {
            dxipl1 = dxi;
        }
        else if (rb == "periodic")
        {
            dxipl1 = chainParticles.at(0).Getposition();
        }
		} else {
			dxipl1 = chainParticles.at(k).Getposition();
		}
		dxi = chainParticles.at(i).Getposition();
		twodxi = 2.0 * dxi;

		fac = dxipl1 + dximn1 - twodxi;
		fac1 = dxipl1 - dxi;
		fac2 = dxi - dximn1;
		fac12 = fac1 * fac1;
		fac22 = fac2 * fac2;
		fac13 = fac1 * fac1 * fac1;
		fac23 = fac2 * fac2 * fac2;
		a = 2.*k1*fac + 3.*k2*(fac12 - fac22) + 4.*k3*(fac13 - fac23);
		mass = chainParticles.at(i).Getmass();
		a = a/mass;
		chainParticles[i].Setaccel(a);


	}

}

double fpuPotential::f_calcPe(std::vector<Particle> &chainParticles)
{
     int i;
     int j;
     double dx;
     double fac;
     double lastSpring;
     double potentialEnergy;
     double totalPotentialEnergy = 0.;
     int systemSize = chainParticles.size();
     for (int i = 0; i < systemSize; i++)
     {

         j = i - 1;
          if (j == -1)
          {
              dx = f_leftBoundaryConditionsPe(chainParticles);;
          }
          else
          {
              dx = chainParticles.at(i).Getposition() - chainParticles.at(j).Getposition();
          }
          fac = dx*dx;

          potentialEnergy = k1*fac + k3*fac*fac + k2*dx*dx*dx;
          Output::writeToPeFile(potentialEnergy, false);
          totalPotentialEnergy +=  potentialEnergy;

     }
     lastSpring = f_rightBoundaryConditionsPe(chainParticles);
     // Technically, it should be k1*(-lastSpring)*(-lastSpring) ...,
     // But the even powers take care of the negative signs. For other potentials, this makes a difference.
     potentialEnergy = k1*lastSpring*lastSpring + k2*(-lastSpring)*(-lastSpring)*(-lastSpring) + k3*lastSpring*lastSpring*lastSpring*lastSpring;
     totalPotentialEnergy += potentialEnergy;
     Output::writeToPeFile(potentialEnergy, true);
     return totalPotentialEnergy;

}




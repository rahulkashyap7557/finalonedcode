#include "Particle.h"


Particle::Particle()
{
    //ctor
    mass = 1.0;
    position = 0.0;
    velocity = 0.0;
    accel = 0.0;
    lboundary = "fixed";
    rboundary = "fixed";
}

Particle::~Particle()
{
    //dtor
}

Particle::Particle(const Particle& other)
{
    //copy ctor
    mass = other.mass;
    position = other.position;
    velocity = other.velocity;
    accel = other.accel;
}

Particle& Particle::operator=(const Particle& rhs)
{
    if (this == &rhs) return *this; // handle self assignment
    //assignment operator
    mass = rhs.mass;
    position = rhs.position;
    velocity = rhs.velocity;
    accel = rhs.accel;

    return *this;
}

double Particle::f_calcKe()
{
    double k = 0.5*mass*velocity*velocity;
    Setke(k);
    return k;
}

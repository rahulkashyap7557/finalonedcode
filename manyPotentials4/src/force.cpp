#include "force.h"

Force::Force()
{
    //ctor
    type = "cosine";
    t1 = 0.;
    t2 = 0.;
    t3 = 0.;
    frequency = 0.;
    ramp = 0.;
    amp = 0.;

}

Force::~Force()
{
    //dtor
}

Force::Force(const Force& other)
{
    //copy ctor
    type = other.type;
    t1 = other.t1;
    t2 = other.t2;
    t3 = other.t3;
    frequency = other.frequency;
    ramp = other.ramp;
    amp = other.amp;
}

Force& Force::operator=(const Force& rhs)
{
    if (this == &rhs) return *this; // handle self assignment
    //assignment operator
    type = rhs.type;
    t1 = rhs.t1;
    t2 = rhs.t2;
    t3 = rhs.t3;
    frequency = rhs.frequency;
    ramp = rhs.ramp;
    amp = rhs.amp;

    return *this;

    return *this;
}

double Force::f_forceCalc(std::vector<Particle> &chainParticles, double tCurrent)
{
    double force;
    if (type == "cosine")
    {
        force = f_cosine(chainParticles, tCurrent);
    }
    else if(type == "sine")
    {
        force = f_sine(chainParticles, tCurrent);
    }
    return force;
}

double Force::f_cosine(vector<Particle> &chainParticles, double tCurrent)
{
    double force;
    force = amp*cos(frequency*tCurrent);
    return force;

}

double Force::f_sine(vector<Particle> &chainParticles, double tCurrent)
{
    double force;
    force = amp*sin(frequency*tCurrent);
    return force;

}


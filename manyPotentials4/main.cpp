#include "Simulation.h"
#include "System.h"
#include "Particle.h"
#include "allIncludes.h"
#include "fpuPotential.h"
using namespace std;

int main()
{

    // Initialize the default Simulation object variables

    int systemSize = 100;
    double dt = 0.01;
    int samplingFrequency = 1/dt;
    double pi = 3.14159;
    int totalTime = 100;

    // Initialize model parameters - default FPUT model
    double k1 = 0.2;
    double k2 = 0.0;
    double k3 = 1.0;
    double k4 = 0.0;
    double k5 = 0.0;

    std::vector<double> mass;
    double defaultMass = 1.0;
    string springType = "fput";

    // Initialize boundary conditions

    string lboundary = "periodic";
    string rboundary = "periodic";


    // Initialize force initialization containers

    std::vector<string> forceType;// = "cosine";
    std::vector<double> forceT1;
    std::vector<double> forceT2;
    std::vector<double> forceT3;
    std::vector<double> forceRamp;
    std::vector<double> forceAmp;
    std::vector<unsigned int> forceParticles;
    std::vector<double> forceGamma;
    std::vector<double> forceFrequency;
    std::vector<unsigned int> dissipationParticles;
    std::vector<double> dissipationStrength;

    double test = 0.0;

    std::string method = "gear5";
    std::vector<unsigned int> perturbedParticles;
    std::vector<string> perturbationType;
    std::vector<double> perturbationAmplitude;

    std::vector<unsigned int> impurityParticles;
    std::vector<double> impurityValue;

    int fileFlag = 0;
    int sampleFlag = 0;

    string line, line1;
    int j = 0;
    int i;
    unsigned int w;
    ifstream parametersFile("parameters.txt");

     if (parametersFile.fail())
     {
         cerr << "Can't open parameters file. Running with default values" << parametersFile << endl;
     }
     else
     {

         while (getline(parametersFile, line))
         {
             cout << "Reading from parameter file" << endl;
             std::vector<std::string> inputList;
             cout << line << endl;
             stringstream ss(line);
             string tmp;
             while(std::getline(ss, tmp, ' '))
             {
                inputList.push_back(tmp);
             }
             string a = inputList.at(0);
             string b = inputList.at(1);
             string c;
             string d;
             string e;
             string f;
             string g;
             string h;

             if (inputList.size() > 2)
             {
                 c = inputList.at(2);
             }
             if (inputList.size() > 3)
             {
                 d = inputList.at(3);
             }
             if (inputList.size() > 4)
             {
                 e = inputList.at(4);
             }
             if (inputList.size() > 5)
             {
                 f = inputList.at(5);
             }
             if (inputList.size() > 6)
             {
                 g = inputList.at(6);
             }
             if (inputList.size() > 7)

             {
                 h = inputList.at(7);
             }

             // Load model type and parameters
             if (a == "model:"){
                springType = b;
                if (inputList.size() > 2){
                    k1 = atof((c.c_str()));
                }
                if (inputList.size() > 3){
                    k2 = atof(d.c_str());
                }
                if (inputList.size() > 4){
                    k3 = atof(e.c_str());
                }
                if (inputList.size() > 5){
                    k4 = atof(f.c_str());
                }
                if (inputList.size() > 6){
                    k5 = atof(g.c_str());
                }

             }

             // Set up forces

             if (a == "force:"){
                if (b == "all"){
                    forceParticles.push_back(0);
                }
                else {
                    forceParticles.push_back(atoi(b.c_str()));
                }
                forceType.push_back(c);
                forceAmp.push_back(atof(d.c_str()));
                forceT1.push_back(atof(e.c_str()));
                forceT2.push_back(atof(f.c_str()));
                forceT3.push_back(atof(g.c_str()));
                forceRamp.push_back(atof(h.c_str()));
                forceFrequency.push_back(2*pi/(atof(f.c_str())));

             }

             // Set up dissipation

             if (a == "dissipation:"){
                if (b == "all"){
                    dissipationParticles.push_back(0);
                }
                else {
                    dissipationParticles.push_back(atoi(b.c_str()));
                }
                dissipationStrength.push_back(atof(c.c_str()));
             }



             // Set up boundaries
             if (a == "boundary:"){
                    if (b == "left"){
                        lboundary = c;
                    }
                    else if (b == "right"){
                        rboundary = c;
                    }
                    else {
                        cout << "Cannot read boundary conditions." << endl;
                        cout << "Continuing simulation with default boundaries" << endl;
                    }

             }



             // Set up initial perturbation
             if(a == "init:"){

                    // Can open initial conditions from file, if name is given

                 if (b == "file"){
                        fileFlag = 1;
                    std::string fileName = c;
                    std::ifstream initFile(c.c_str());
                    if (initFile.fail()){
                        cerr << "Cannot open init file. Starting with default initial conditions" << initFile << endl;
                    }
                    else{
                        int fileLineC = 0;
                        int partic;
                        std::vector<std::string> initList(3);
                        while (getline(initFile, line1)){
                            stringstream ss1(line1);
                            string tmpx, tmpv, tmpa;
                            ss1 >> tmpx >> tmpv >> tmpa;
                            partic = fileLineC + 1;

                            perturbedParticles.push_back(partic);
                            perturbationType.push_back("pos");
                            perturbationAmplitude.push_back(atof(tmpx.c_str()));
                            perturbedParticles.push_back(partic);
                            perturbationType.push_back("vel");
                            perturbationAmplitude.push_back(atof(tmpv.c_str()));
                            perturbedParticles.push_back(partic);
                            perturbationType.push_back("acc");
                            perturbationAmplitude.push_back(atof(tmpa.c_str()));
                            fileLineC += 1;

                        }
                        systemSize = partic;

                    }

                 }

                 else {
                    perturbedParticles.push_back(atoi(b.c_str()));
                    perturbationType.push_back(c);
                    if (d == "random"){
                       double low = atof(e.c_str());
                       double high = atof(f.c_str());
                       double num = low + (high - low)*(static_cast<double>(rand() % 100))/100;
                       perturbationAmplitude.push_back(num);
                    }
                    else {
                    perturbationAmplitude.push_back(atof(d.c_str()));

                  }

                }

             }

             // Set up masses
             if (a == "mass:"){
                    if (b == "all"){
                        impurityParticles.push_back(0);
                    }
                    else{
                        impurityParticles.push_back(atoi(b.c_str()));
                    }
                    impurityValue.push_back(atof(c.c_str()));
             }

             // Set up total run time
             if (a == "totaltime:"){
                totalTime = atof(b.c_str());
             }

             // Set up system size
             if (a == "systemsize:"){
                 systemSize = atof(b.c_str());
             }

             // Set up time step
             if (a == "timestep:"){
                dt = atof(b.c_str());
             }

             // Set up sampling steps
             if (a == "sampling:"){
                samplingFrequency = atoi(b.c_str());
                sampleFlag = 1;
             }

             // Set up integration method
             if (a == "method:"){

                method = b;
             }
             j++;



         }

     }

      ofstream outputFile;
     outputFile.open("log.txt");
     outputFile << "FPUT system with harmonic and quartic potential" << '\n';
     outputFile << "k1:" << '\t' << k1 << '\n';
     outputFile << "k2:" << '\t' << k2 << '\n';
     //outputFile << "amplitude:" << '\t' << amplitude << '\n';
     outputFile << "time step:" << '\t' << dt << '\n';
     outputFile << "total recorded time:" << '\t' << totalTime << '\n';
     outputFile << "system size:" << '\t' << systemSize  << '\n';
     outputFile << "method:" << '\t' << method << '\n';

     outputFile << " velocity perturbations of size 'amplitude' seeded in following particles" << '\n';

     for (i = 0; i < perturbedParticles.size();i++){

          outputFile << "particle:" << '\t' << perturbedParticles.at(i) << '\t' << perturbationType.at(i) << '\t' << perturbationAmplitude.at(i) << '\n';
     }

     outputFile.close();





    // Create particle object

    std::vector<Particle> chainParticles(systemSize);
    double defaultx = 0.;
    double defaultv = 0.;
    double defaulta = 0.;

    // Next create an object with info about the springs

    int numSprings;

    numSprings = systemSize + 1;

    if (lboundary == "periodic")
        {
            rboundary = lboundary;
            numSprings = systemSize;
        }
    if (rboundary == "periodic")
    {
        lboundary = rboundary;
        numSprings = systemSize;
    }


    // Initialize positions, velocities and accelerations

    for (i = 0; i < systemSize; i++)
    {
        chainParticles[i].Setvelocity(defaultv);
        chainParticles[i].Setposition(defaultx);
        chainParticles[i].Setaccel(defaulta);
        chainParticles[i].Setmass(defaultMass); // change mass into vector for impurities
        chainParticles[i].Setlboundary(lboundary);
        chainParticles[i].Setrboundary(rboundary);

    }


    double amp;
    if (perturbedParticles.size() > 0){
    for (i = 0; i < perturbedParticles.size(); i++)
    {
        w = perturbedParticles.at(i);
        amp = perturbationAmplitude.at(i);
        if (w - 1 < systemSize){
        if (perturbationType.at(i) == "vel")
        {
            chainParticles[w-1].Setvelocity(amp);
        }
        if (perturbationType.at(i) == "pos")
        {
            chainParticles[w-1].Setposition(amp);
        }
        if (perturbationType.at(i) == "acc")
        {
            chainParticles[w-1].Setaccel(amp);
        }
        }


    }
    }




std::string type;
double t1;
double t2;
double t3;
double frequency;
double ramp;


// Initialize force object to default values
std::vector<Force> force(systemSize);
for (int i = 0; i < systemSize; i++)
{
    force[i].Settype("cosine");
    force[i].Sett1(0.);
    force[i].Sett2(0.);
    force[i].Sett3(0.);
    force[i].Setfrequency(0.);
    force[i].Setramp(0.);
    force[i].Setamp(0.);
    force[i].Setgamma(0.);
}


// Retrieve initial conditions from values in parameter file
if (forceParticles.size() > 0){
for (int i = 0; i < forceParticles.size(); i++)
{
    w = forceParticles.at(i);
    type = forceType.at(i);
    amp = forceAmp.at(i);
    t1 = forceT1.at(i);
    t2 = forceT2.at(i);
    t3 = forceT3.at(i);
    frequency = forceFrequency.at(i);
    ramp = forceRamp.at(i);
    amp = forceAmp.at(i);

    if (w == 0)
    {
        for (j = 0; j < systemSize; j++){
            force[j].Settype(type);
            force[j].Sett1(t1);
            force[j].Sett2(t2);
            force[j].Sett3(t3);
            force[j].Setfrequency(frequency);
            force[j].Setramp(ramp);
            force[j].Setamp(amp);
        }
    }
    else
    {
        j = w-1;
        force[j].Settype(type);
        force[j].Sett1(t1);
        force[j].Sett2(t2);
        force[j].Sett3(t3);
        force[j].Setfrequency(frequency);
        force[j].Setramp(ramp);
        force[j].Setamp(amp);
    }
}
}


// Initialize dissipation object to default values
for (int i = 0; i < systemSize; i++)
{
    force[i].Setgamma(0.);
}

// Retrieve dissipation values from dissipation object
if(dissipationParticles.size() > 0){
for (int i = 0; i < dissipationParticles.size(); i++)
{
    w = dissipationParticles.at(i);
    amp = dissipationStrength.at(i);

    if (w == 0)
    {
        for (j = 0; j < systemSize; j++){
            force[j].Setgamma(amp);
        }
    }
    else
    {
        j = w-1;
        force[j].Setgamma(amp);
    }
}
}


// Set mass is 1.0

for (i = 0; i < systemSize; i++)
{
    mass.push_back(defaultMass);
}
// Retrieve values of impurities if there are any
if (impurityParticles.size() > 0)
{
    for (i = 0; i < impurityParticles.size(); i++)
        {
            w = impurityParticles.at(i);
            amp = impurityValue.at(i);
            if (w == 0)
            {
                for (j = 0; j < systemSize; j++)
                {
                    chainParticles[j].Setmass(amp);
                }
            }
            else
            {
                j = w-1;
                chainParticles[j].Setmass(amp);
            }

        }


}

// Check that mass is greater than 0

for (i = 0; i < systemSize; i++)
{
    amp = chainParticles.at(i).Getmass();
    if (amp <= 0.00000000001)
    {
        chainParticles[i].Setmass(defaultMass);
        cout << "Mass at %d th particle was less than or equal to 0." << endl;
    }
}

    // Create simulation object

    Simulation simParams;
    if (sampleFlag == 0)
    {
        samplingFrequency = 1/dt;
    }



    simParams.SettimeStep(dt);
    simParams.SetsamplingFrequency(samplingFrequency);
    simParams.SettotalTime(totalTime);
    simParams.Setmethod(method);
    simParams.SetsystemSize(systemSize);

    // Create Spring object

    // Create all kinds of Spring Objects

    std::vector<fpuPotential> fpuObject(1);
    std::vector<todaPotential> todaObject(1);
    std::vector<morsePotential> morseObject(1);
    std::vector<lennardJonesPotential> lennardJonesObject(1);


// Check which model is chosen and accordingly start simulation
    if (springType == "toda")
    {
        todaObject[0].Setk1(k1);
        todaObject[0].Setk2(k2);

        simParams.f_startSim(chainParticles, todaObject, force);
    }
    else if (springType == "fput")
    {

        fpuObject[0].Setk1(k1);
        fpuObject[0].Setk2(k2);

        simParams.f_startSim(chainParticles, fpuObject, force);
    }
    else if (springType == "morse")
    {
        morseObject[0].Setk1(k1);
        morseObject[0].Setk2(k2);

        simParams.f_startSim(chainParticles, morseObject, force);
    }
    else if (springType == "lennardjones")
    {
        lennardJonesObject[0].Setk1(k1);
        lennardJonesObject[0].Setk2(k2);

        simParams.f_startSim(chainParticles, lennardJonesObject, force);
    }
    else
    {
        cout << "No recognized model type specified. Exiting program." << endl;
        return 0;
    }

    return 0;
}

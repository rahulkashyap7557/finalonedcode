#ifndef LENNARDJONESPOTENTIAL_H
#define LENNARDJONESPOTENTIAL_H
#include <algorithm>
#include <cstdlib>
#include <string>
#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <cassert>
#include <iterator>
#include <iomanip>
#include <sstream>
#include "Particle.h"
#include "System.h"

class lennardJonesPotential : public System {
    public:
        /** Default constructor */
        lennardJonesPotential();
        lennardJonesPotential(Particle chainParticles);


        /** Default destructor */
        virtual ~lennardJonesPotential();
        /** Copy constructor
         *  \param other Object to copy from
         */
        lennardJonesPotential(const lennardJonesPotential& other);
        /** Assignment operator
         *  \param other Object to assign from
         *  \return A reference to this
         */
        lennardJonesPotential& operator=(const lennardJonesPotential& other);

        //
        double Getk1() { return k1; }
        /** Set mass
         * \param val New value to set
         */
        void Setk1(double val) { k1 = val; }
        /** Access position
         * \return The current value of position
         */

        double Getk2() { return k2; }
        /** Set mass
         * \param val New value to set
         */
        void Setk2(double val) { k2 = val; }
        /** Access position
         * \return The current value of position
         */




        void f_accel(std::vector<Particle> &chainParticles);

        double f_calcPe(std::vector<Particle> &chainParticles);

        double Getpe(){ return pe;}

        void Setpe(double val){ pe = val; }



    protected:

    private:
        double k1;
        double k2;
        double pe;

};

#endif // LENNARDJONESPOTENTIAL_H



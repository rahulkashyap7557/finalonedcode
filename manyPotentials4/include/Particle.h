#ifndef PARTICLE_H
#define PARTICLE_H
#include <algorithm>
#include <cstdlib>
#include <string>
#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <cassert>
#include <iterator>
#include <iomanip>
#include <sstream>

using namespace std;
//#include "allIncludes.h"


class Particle
{
    public:
        /** Default constructor */
        Particle();
        /** Default destructor */
        virtual ~Particle();
        /** Copy constructor
         *  \param other Object to copy from
         */
        Particle(const Particle& other);
        /** Assignment operator
         *  \param other Object to assign from
         *  \return A reference to this
         */
        Particle& operator=(const Particle& other);

        /** Access mass
         * \return The current value of mass
         */
        double Getmass() { return mass; }
        /** Set mass
         * \param val New value to set
         */
        void Setmass(double val) { mass = val; }
        /** Access position
         * \return The current value of position
         */
        double Getposition() { return position; }
        /** Set position
         * \param val New value to set
         */
        void Setposition(double val) { position = val; }
        /** Access velocity
         * \return The current value of velocity
         */
        double Getvelocity() { return velocity; }
        /** Set velocity
         * \param val New value to set
         */
        void Setvelocity(double val) { velocity = val; }
        /** Access accel
         * \return The current value of accel
         */
        double Getaccel() { return accel; }
        /** Set accel
         * \param val New value to set
         */
        void Setaccel(double val) { accel = val; }

        double Getke(){return ke; }

        void Setke(double val){ ke = val; }

        string Getlboundary() { return lboundary; }

        /** Set left boundary
         * \param val New value to set
         */
        void Setlboundary(string val) { lboundary = val; }

                /** Access boundary
         * \return The current value of boundary
         */
        string Getrboundary() { return rboundary; }

        /** Set boundary
         * \param val New value to set
         */
        void Setrboundary(string val) { rboundary = val; }

        double f_calcKe();





    protected:

    private:
        double mass; //!< Member variable "mass"
        double position; //!< Member variable "position"
        double velocity; //!< Member variable "velocity"
        double accel; //!< Member variable "acceleration"
        double ke; //!< Member variable "kinetic energy"
        string lboundary;
        string rboundary ;
};

#endif // PARTICLE_H

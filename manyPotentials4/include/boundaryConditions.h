#ifndef BOUNDARYCONDITIONS_H
#define BOUNDARYCONDITIONS_H
#include <algorithm>
#include <cstdlib>
#include <string>
#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <cassert>
#include <iterator>
#include <iomanip>
#include <sstream>
#include "allIncludes.h"

using namespace std;

double f_leftBoundaryConditionsAccel(std::vector<Particle> &chainParticles);
double f_rightBoundaryConditionsAccel(std::vector<Particle> &chainParticles);
double f_leftBoundaryConditionsPe(std::vector<Particle> &chainParticles);
double f_rightBoundaryConditionsPe(std::vector<Particle> &chainParticles);
#endif // BOUNDARYCONDITIONS_H



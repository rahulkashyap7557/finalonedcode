#ifndef ALLINCLUDES_H_INCLUDED
#define ALLINCLUDES_H_INCLUDED

#include <algorithm>
#include <cstdlib>
#include <string>
#include <cmath>
#include <limits>
#include <fstream>
#include <iostream>
#include <vector>
#include <cassert>
#include <iterator>
#include <iomanip>
#include <sstream>
#include <stdlib.h>
#include "Output.h"
#include "Particle.h"
#include "fpuPotential.h"
#include "todaPotential.h"
#include "morsePotential.h"
#include "lennardJonesPotential.h"
#include "Simulation.h"
#include "System.h"
#include "boundaryConditions.h"

void f_inputFile(double &k1, double &k2, double &amplitude);




#endif // ALLINCLUDES_H_INCLUDED

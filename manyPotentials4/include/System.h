#ifndef SYSTEM_H
#define SYSTEM_H

#include <algorithm>
#include <cstdlib>
#include <string>
#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <cassert>
#include <iterator>
#include <iomanip>
#include <sstream>

using namespace std;


class System
{
    public:
        /** Default constructor */
        System();
        /** Default destructor */
        virtual ~System();
        /** Copy constructor
         *  \param other Object to copy from
         */
        System(const System& other);
        /** Assignment operator
         *  \param other Object to assign from
         *  \return A reference to this
         */
        System& operator=(const System& other);

        /** Access left boundary
         * \return The current value of boundary
         */





    protected:


    private:


};

#endif // SYSTEM_H

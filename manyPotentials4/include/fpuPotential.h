#ifndef FPUPOTENTIAL_H
#define FPUPOTENTIAL_H
#include <algorithm>
#include <cstdlib>
#include <string>
#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <cassert>
#include <iterator>
#include <iomanip>
#include <sstream>
#include "Particle.h"
#include "System.h"
//#include "allIncludes.h"
class fpuPotential : public System {
    public:
        /** Default constructor */
        fpuPotential();
        fpuPotential(Particle chainParticles);


        /** Default destructor */
        virtual ~fpuPotential();
        /** Copy constructor
         *  \param other Object to copy from
         */
        fpuPotential(const fpuPotential& other);
        /** Assignment operator
         *  \param other Object to assign from
         *  \return A reference to this
         */
        fpuPotential& operator=(const fpuPotential& other);

        //
        double Getk1() { return k1; }
        /** Set mass
         * \param val New value to set
         */
        void Setk1(double val) { k1 = val; }
        /** Access position
         * \return The current value of position
         */

        double Getk2() { return k2; }
        /** Set mass
         * \param val New value to set
         */
        void Setk2(double val) { k2 = val; }
        /** Access position
         * \return The current value of position
         */

         double Getk3() { return k3; }
        /** Set mass
         * \param val New value to set
         */
        void Setk3(double val) { k3 = val; }
        /** Access position
         * \return The current value of position
         */




        void f_accel(std::vector<Particle> &chainParticles);

        double f_calcPe(std::vector<Particle> &chainParticles);



        double Getpe(){ return pe;}

        void Setpe(double val){ pe = val; }

//        void f_acceleration();
//        void f_energies();


    protected:

    private:
        double k1;
        double k2;
        double k3;
        double pe;

};

#endif // POTENTIAL_H

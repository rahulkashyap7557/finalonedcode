# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

A work in progress. This code would enable any user to run molecular dynamics simulations for a vast variety of nonlinear 1D many-body spring mass systems with minimal effort.
The user should just be able to pick and choose which features/system he/she needs and run the .exe to get precise scientific data for that system.
The features are as follows. Please note that some features are still being added. 

Potentials: FPUT, Toda, Morse and Lennard-Jones (Plnning Frenkel-Kontorova and Sievers cantilever system)
Methods: Gear 5th order, Velocity Verlet (planning RK4, Adaptive RK)
Boundary Conditions: open, fixed, periodic (planning Dissipative Boundaries)
Force: Sine, Cosine (planning Sawtooth, Square Wave, Chirping)
Dissipation: Velocity dependent
Parameter file interface with predefined commands.

### How do I get set up? ###
 
Download the code and open the code blocks project. You can make some changes to main.cpp for your required system configuration.
A parameter file that allows user to set commands is supported and list of all commands is available in the user manual provided with the software.
You can run the code in code blocks software or by simply downloading the .exe file with a filled out parameter file in the same folder. 
You will need a C++ compiler to build the project if you make any changes to it. 

### Who do I talk to? ###

Rahul Kashyap
Email me at rahulkashyap7557@gmail.com